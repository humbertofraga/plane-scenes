import { Component, OnInit } from '@angular/core';
import { CardsService } from '../services/cards.service';
import { MtgCard } from '../services/scryfall.service';
import { Router } from '@angular/router';
declare var window: any;

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.sass']
})
export class GameComponent implements OnInit {

  deck: MtgCard[] = [];
  active: MtgCard[] = [];
  revealed: MtgCard[] = [];

  private faces = [
    "bi-square",
    "bi-square",
    "bi-square",
    "bi-square",
    "mana ms-planeswalker",
    "mana ms-chaos"
  ];
  last_dice_result = "bi-square";
  modal_dice: any;

  constructor(
    private cardsService: CardsService,
    private route: Router
  ) {}

  ngOnInit(): void {
    this.start(this.cardsService.selected_cards);
  }

  private start(selection: Set<MtgCard>) {
    console.log(`Received ${selection.size} cards`);
    if (selection.size == 0) {
      this.route.navigate(['/']);
    }
    this.deck = Array.from(selection);
    this.shuffle(this.deck);
    
    this.planeswalk();
    this.prepare_modal();
  }

  private prepare_modal() {
    var modal_dice_el = document.getElementById("modal_dice");
    this.modal_dice = new window.bootstrap.Modal(
      modal_dice_el
    );
    if (modal_dice_el !== null) {
      modal_dice_el.addEventListener('hidden.bs.modal', (_event: Event) => {
        if (this.last_dice_result == "mana ms-planeswalker") {
          this.planeswalk();
        }
      });
    }
  }

  getDiceResult(): string {
    return this.faces[Math.floor(Math.random() * this.faces.length)];
  }

  shuffle(array: Array<any>) {
    for (let i = array.length -1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i+1));
      [array[i], array[j]] = [array[j], array[i]];
    }
  }

  planeswalk() {
    this.deck.push(...this.active);
    this.active = [this.deck[0]];
    this.deck.splice(0, 1);
  }

  add() {
    this.active.push(this.deck[0]);
    this.deck.splice(0, 1);
  }

  reveal() {
    this.revealed.push(this.deck[0]);
    this.deck.splice(0, 1);
  }

  discard_revealed() {
    this.shuffle(this.revealed);
    this.deck.push(...this.revealed);
    this.revealed = [];
  }

  discard(card_id: string) {
    console.log(`Discarding ${card_id}`);
    this.active.forEach((c, i) => {
      console.log(`...checking ${c.id}`);
      if (c.id == card_id) {
        this.active.splice(i, 1);
        this.deck.push(c);
      }
    });
  }

  move_to_deck(card_id: string) {
    this.revealed.forEach((c, i) => {
      if (c.id == card_id) {
        this.revealed.splice(i, 1);
        this.deck.unshift(c);
      }
    });
  }

  move_to_active(card_id: string) {
    console.log(`Activating ${card_id}`);
    this.revealed.forEach((c, i) => {
      console.log(`...checking ${c.id}`);
      if (c.id == card_id) {
        this.revealed.splice(i, 1);
        this.active.push(c);
      }
    });
  }

  has_phenomenon() : boolean {
    return this.active.some((c) => c.type_line == "Phenomenon");
  }

  rollDice() {
    if ((this.active.length == 0) || (this.active.some((c) => c.type_line == "Phenomenon"))) {
      this.planeswalk();
    } else {
      this.last_dice_result = this.getDiceResult();
      this.modal_dice.show();
    }
  }
}
