import { Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { CardsService } from '../services/cards.service';
import { MtgCard, MtgSet } from '../services/scryfall.service';
import { CardComponent } from '../card/card.component';

@Component({
  selector: 'app-cardlist',
  templateUrl: './cardlist.component.html',
  styleUrls: ['./cardlist.component.sass']
})
export class CardlistComponent implements OnInit {

  @ViewChildren('card') cards!: QueryList<CardComponent>

  constructor(
    public cardsService: CardsService
  ) {}

  ngOnInit(): void {
    this.cardsService.getCards();
  }

  onSetCheckedChanged(event: any) {
    if (event.target.checked == true) {
      this.cardsService.selected_sets.add(event.target.value);
    } else {
      this.cardsService.selected_sets.delete(event.target.value);
    }
    this.cardsService.filterCardList();
  }

  onShowPhenomenonChanged(event: any) {
    this.cardsService.show_phenomenon = event.target.checked;
    this.cardsService.filterCardList();
  }

  onCardSelected(card_component: CardComponent) {
    if (card_component.isSelected) {
      this.cardsService.selected_cards.add(card_component.card);
    } else {
      this.cardsService.selected_cards.delete(card_component.card);
    }
  }

  onSelectAllClicked() {
    this.cards.forEach((e) => {
      e.isSelected = true;
      this.cardsService.selected_cards.add(e.card);
    });
  }

  onSelectNoneClicked() {
    this.cards.forEach((e) => {
      e.isSelected = false;
      this.cardsService.selected_cards.delete(e.card);
    })
  }
}
