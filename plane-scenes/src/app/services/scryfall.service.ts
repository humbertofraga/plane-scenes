import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

export interface MtgCard {
  id: string,
  name: string,
  set: string,
  type_line: string,
  oracle_text: string,
  image_uris: {
    art_crop: ''
  }
}

export interface MtgSet {
  id: string,
  name: string,
  code: string,
  parent_set_code: string,
  set_type: string,
  released_at: string,
  card_count: number,
  icon_svg_url: string
}

export interface ScryfallCardResponse {
  object: string,
  total_cards: number,
  has_more: boolean,
  data: MtgCard[]
}

export interface ScryfallSetResponse {
  object: string,
  has_more: boolean,
  data: MtgSet[]
}

@Injectable({
  providedIn: 'root'
})
export class ScryfallService {
  private url = "https://api.scryfall.com";

  constructor(private http: HttpClient) { }

  getCards(query: string) {
    const options = query?
      { params: new HttpParams().set('q', query) } : {}
    return this.http.get<ScryfallCardResponse>(`${this.url}/cards/search`, options);
  }

  getSets() {
    return this.http.get<ScryfallSetResponse>(`${this.url}/sets`);
  }

  getSet(code: string) {
    return this.http.get<ScryfallSetResponse>(`${this.url}/sets/${code}`);
  }
}
