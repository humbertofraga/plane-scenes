import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { MtgCard, MtgSet, ScryfallService } from './scryfall.service';

@Injectable({
  providedIn: 'root'
})
export class CardsService {

  constructor(
    private scryfall: ScryfallService
  ) {
    console.log('Creating CardsService');
  }

  sets: MtgSet[] = [];
  cards: MtgCard[] = [];
  filtered_cards: MtgCard[] = [];
  selected_sets = new Set<string>();
  show_phenomenon: boolean = true;
  selected_cards = new Set<MtgCard>();

  getCards(sets? : string[]) {
    let query = "(t:plane OR t:phenomenon)";
    if (sets !== undefined) {
      if (sets.length == 1) {
        query += ` set:${sets[0]}`
      } else if (sets.length > 1) {
        query += " " + sets.join(" OR ");
      }
      console.log(query);
    }
    this.scryfall.getCards(query)
      .subscribe(results => this.updateCardList(results.data));
  }

  getSets(codes: Set<string>) {
    this.scryfall.getSets()
      .subscribe(results => this.filterSetList(results.data, codes));
  }

  updateCardList(results: MtgCard[]) {
    this.cards = results;
    this.filtered_cards = Array.from(results);
    let resultSets = new Set<string>();
    results.forEach(function(card) {
      resultSets.add(card.set);
    });
    this.selected_cards = new Set<MtgCard>(this.cards);
    if (this.sets.length == 0) {
      this.getSets(resultSets);
    }
  }

  filterCardList() {
    this.filtered_cards = this.cards
      .filter( card => this.selected_sets.has(card.set))
      .filter( card => this.show_phenomenon ? card : (card.type_line != "Phenomenon"));
  }

  filterSetList(results: MtgSet[], codes: Set<string>) {
    this.sets = results.filter(set => codes.has(set.code));
    this.sets.forEach(_set => { this.selected_sets.add(_set.code) });
  }
}
