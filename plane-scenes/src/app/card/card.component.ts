import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MtgCard } from '../services/scryfall.service';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.sass']
})
export class CardComponent {
  @Input()
  card!: MtgCard;

  @Input()
  isSelection: boolean = false;

  @Input()
  isRevealed: boolean = false;

  @Input()
  isActive: boolean = false;

  @Output()
  btn_discard_clicked = new EventEmitter<string>();

  @Output()
  btn_move_to_deck_clicked = new EventEmitter<string>();

  @Output()
  btn_move_to_active_clicked = new EventEmitter<string>();

  @Input() isSelected: boolean = true;

  @Output()
  cb_selected = new EventEmitter<CardComponent>();

  getImage() {
    return this.card?.image_uris.art_crop;
  }

  selected_toggle(event: any) {
    this.isSelected = !this.isSelected;
    this.cb_selected.emit(this);
  }

  discard_clicked() {
    this.btn_discard_clicked.emit(this.card.id);
  }

  move_to_deck_clicked() {
    this.btn_move_to_deck_clicked.emit(this.card.id);
  }

  move_to_active_clicked() {
    this.btn_move_to_active_clicked.emit(this.card.id);
  }
}
