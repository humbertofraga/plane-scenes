import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CardlistComponent } from './cardlist/cardlist.component';
import { CardComponent } from './card/card.component';
import { NewLinesToParagraphsPipe } from './pipes/new-lines-to-paragraphs.pipe';
import { ManaSymbolsPipe } from './pipes/mana-symbols.pipe';
import { GameComponent } from './game/game.component';
import { CardsService } from './services/cards.service';

@NgModule({
  declarations: [
    AppComponent,
    CardlistComponent,
    CardComponent,
    NewLinesToParagraphsPipe,
    ManaSymbolsPipe,
    GameComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [CardsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
