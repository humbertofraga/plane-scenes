import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'newLinesToParagraphs'
})
export class NewLinesToParagraphsPipe implements PipeTransform {

  transform(value: string): string {
    return value.replace(/(.*)\n/g, '<p>$1</p>');
  }

}
