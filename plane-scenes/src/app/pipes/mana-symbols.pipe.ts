import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'manaSymbols'
})
export class ManaSymbolsPipe implements PipeTransform {

  transform(value: string): string {
    return value.replace(/{(.|CHAOS)}/g, this.iconSymbol);
  }

  private iconSymbol(match: string, p1: string): string {
    var s = '';
    switch(p1) {
      case 'T':
        s = 'tap';
        break;
      case 'CHAOS':
        s = 'chaos';
        break;
      default:
        s = `${p1}`.toLowerCase();
        break;
    }
    return `<i class='ms ms-${s}'></i>`
  }

}
