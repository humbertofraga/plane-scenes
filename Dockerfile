FROM node:18-alpine

RUN npm install -g @angular/cli

USER node

WORKDIR /home/node/app

COPY --chown=node:node plane-scenes/package*.json ./

RUN npm install

COPY --chown=node:node plane-scenes/ ./

COPY entrypoint.sh /usr/local/bin/

EXPOSE 4200

ENTRYPOINT [ "/usr/local/bin/entrypoint.sh" ]
CMD [ "ng", "serve", "--host", "0.0.0.0" ]
