FROM node:18-alpine AS builder

USER node

WORKDIR /home/node/app

COPY --chown=node:node plane-scenes/ ./

RUN npm install

ENV PATH=./node_modules/.bin:$PATH

RUN ng build --base-href /plane-scenes/

RUN mkdir public && \
    mv dist/plane-scenes/* public

FROM nginx:1.25-alpine

COPY --from=builder /home/node/app/public/ /usr/share/nginx/html/plane-scenes

RUN ls -lha /usr/share/nginx/html